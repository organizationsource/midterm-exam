# Midterm-Exam

_Total: 100%_

- **Primary Content** - 30% 
- **Animations** - 10% 
- **Responsive** - 40%
- **Creativity** - 20%


> Websume: Primary Content
- [ ] Full Name
- [ ] Date of Birth
- [ ] Job Position or Field
- [ ] Contact Number
- [ ] Contact Address
- [ ] Gmail / LSU email
- [ ] Social Media Links / Github Account
- [ ] Education
- [ ] Professional Summary
- [ ] Skills
- [ ] Certificates / Workshops Attended / Seminars / etc.
- [ ] Leadership
- [ ] Work History (optional)
- [ ] Portfolio / Project Outputs / Other related areas related to Fullstack or Software Development
- [ ] References


## Branch name must be from the assigned ticket. Visit Linear App : https://linear.app/webtech2/team/WEB/active

## This is not just an ordinary Resume it is your Professional Website Portfolio and Exhibition of your Academic Achievements.

## Required Software Tools : Ionic Vue Component Based in a Single Page Application with SASS.
